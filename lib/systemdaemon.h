#ifndef SYSTEMDAEMON_H
#define SYSTEMDAEMON_H

#include <QString>

class SystemDaemon
{
public:
    SystemDaemon();
    QString Name;
    QString Description;
    QString State;
    QString Preset;
};

#endif // SYSTEMDAEMON_H
