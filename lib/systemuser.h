#ifndef SYSTEMUSER_H
#define SYSTEMUSER_H

#include <QString>

class SystemUser
{
public:
    SystemUser();
    ~SystemUser();
    QString Username;
    int UID;
    int GID;
    QString Fullname;
    QString HomeDirectory;
    QString LoginShell;
    QString Groups;
};

#endif // SYSTEMUSER_H
