#include "systeminformation.h"
#include "qregularexpression.h"
#include "systemuser.h"

#include <QProcess>
#include <QString>
#include <fstream>
#include <array>
#include <bits/stdc++.h>
#include <string>

using std::string;
using std::ifstream;

SystemInformation::SystemInformation()
{
    users = new QList<SystemUser>();
    daemons = new QList<SystemDaemon>();
    this->readReleaseFile();
    this->readProcVerFile();
    this->readPasswdFile();
    this->readUserGroups();
    this->readDaemons();
    this->readHost();
    this->readConfigs();
    this->doAudit();
    this->doTreeCheck();
}

SystemInformation::~SystemInformation()
{
    delete users;
    delete daemons;
}

void SystemInformation::readReleaseFile()
{
    /*
     * 0 - Name
     * 1 - Pretty Name
     * 2 - ANSI Color
     * 3 - HOME URL
     * 4 - DOCUMENTATION URL
     * 5 - SUPPORT URL
     * 6 - BUG REPORT URL
     * 7 - PRIVACY POLICY URL
     */
    string lines[8];

    ifstream release("/etc/os-release");
    if (release.is_open())
    {
        int i = 0;
        string dummy;
        string value;
        while (std::getline(std::getline(release, dummy, '"'), value, '"'))
        {
            lines[i] = value;
            i++;
        }
    }
    else
    {
        throw "No /etc/release file";
    }

    this->DistribName       = QString::fromStdString(lines[1]);
    this->DistribURL        = QString::fromStdString(lines[3]);
    this->DocumentationURL  = QString::fromStdString(lines[4]);
    this->SupportURL        = QString::fromStdString(lines[5]);
    this->BugReportURL      = QString::fromStdString(lines[6]);
    this->PrivacyPolicyURL  = QString::fromStdString(lines[7]);
}

void SystemInformation::readProcVerFile()
{
    ifstream version("/proc/version");
    if (version.is_open())
    {
       string kernel;
       std::getline(version, kernel);
       this->KernelVer = QString::fromStdString(kernel);
    }
    else
    {
        throw "No /proc/version file";
    }
}

void SystemInformation::readPasswdFile()
{
    ifstream passwd("/etc/passwd");
    if (passwd.is_open())
    {
        string l;
        while(std::getline(passwd, l))
        {
            SystemUser u;
            std::stringstream ss(l);
            QList<QString> tokens;
            while(!ss.eof())
            {
                string w;
                getline(ss, w, ':');
                tokens.append(QString::fromStdString(w));
            }
            // 0 - login : 1 - password : 2 - UID : 3 - GID : 4 - GECOS : 5 - home : 6 - shell
            try {
                u.Username = tokens[0];
                u.UID = std::stoi(tokens[2].toStdString());
                u.GID = std::stoi(tokens[3].toStdString());
                u.Fullname = tokens[4];
                u.HomeDirectory = tokens[5];
                u.LoginShell = tokens[6];
            } catch(...) {
                throw "Problem reading /etc/passwd file";
            }

            users->append(u);
        }
    }
    else
    {
        throw "No /etc/passwd file";
    }
}

void SystemInformation::readUserGroups()
{
    for (auto &u : *users)
    {
        QProcess proc;
        proc.start("groups", QStringList() << u.Username);
        proc.waitForFinished();

        u.Groups = QString(proc.readAllStandardOutput());
    }
}

void SystemInformation::readDaemons()
{
    QProcess proc;
    proc.start("systemctl", QStringList() << "list-unit-files");
    proc.waitForFinished();
    QStringList units = QString(proc.readAllStandardOutput()).split('\n');
    units.removeAt(0);
    units.removeAll({});
    units.removeAt(units.count() - 1);

    for (auto &entry : units)
    {
        SystemDaemon d;
        QStringList attr = entry.split(' ');
        attr.removeAll({});
        if (attr.count() > 2)
        {
            d.Name = attr[0];
            d.State = attr[1];
            d.Preset = attr[2];
        }

        QProcess proc;
        if (d.Name.startsWith('-'))
        {
            proc.start("systemctl", QStringList() << "status" << "--" << d.Name);
        }
        else
        {
            proc.start("systemctl", QStringList() << "status" << d.Name);
        }
        proc.waitForFinished();
        QStringList status = QString(proc.readAllStandardOutput()).split('\n');
        QString desc = status[0].split('-').back();
        d.Description = desc;
        daemons->append(d);
    }
}

void SystemInformation::readHost()
{
    QProcess proc;
    proc.start("cat", QStringList() << "/etc/hostname");
    proc.waitForFinished();
    this->Host = proc.readAllStandardOutput();
}

void SystemInformation::readConfigs()
{
    QProcess proc;
    proc.start("ls", QStringList() << "-m" << "/etc/");
    proc.waitForFinished();
    this->Configs = proc.readAllStandardOutput();
}

void SystemInformation::doAudit()
{
    QProcess proc;
    proc.start("checksec", QStringList() << "--proc-all");
    proc.waitForFinished();
    QString res = proc.readAllStandardOutput();
    QRegularExpression expr(QStringLiteral("\x1b\[[0-9;]*m"));
    this->Audit = res.replace(expr, "");
}

void SystemInformation::doTreeCheck()
{
    QProcess proc;
    proc.start("tree", QStringList() << "/" << "-L" << "2");
    proc.waitForFinished();
    this->FileTree = proc.readAllStandardOutput();
}

QList<SystemUser> *SystemInformation::getUsers() const
{
    return users;
}

QList<SystemDaemon> *SystemInformation::getDaemons() const
{
    return daemons;
}
