#ifndef SYSTEMINFORMATION_H
#define SYSTEMINFORMATION_H

#include <QString>
#include <QList>
#include "systemuser.h"
#include "systemdaemon.h"

class SystemInformation
{
public:
    SystemInformation();
    ~SystemInformation();
    QString DistribName;
    QString DistribURL;
    QString DocumentationURL;
    QString SupportURL;
    QString BugReportURL;
    QString PrivacyPolicyURL;
    QString KernelVer;
    QString Host;
    QString Configs;
    QString Audit;
    QString FileTree;

    QList<SystemUser>* getUsers() const;
    QList<SystemDaemon>* getDaemons() const;

private:
    void readReleaseFile();
    void readProcVerFile();
    void readPasswdFile();
    void readUserGroups();
    void readDaemons();
    void readHost();
    void readConfigs();
    void doAudit();
    void doTreeCheck();
    QList<SystemUser>* users;
    QList<SystemDaemon>* daemons;
};

#endif // SYSTEMINFORMATION_H
