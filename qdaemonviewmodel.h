#ifndef QDAEMONVIEWMODEL_H
#define QDAEMONVIEWMODEL_H

#include <QModelIndex>
#include <QStringList>
#include "lib/systemdaemon.h"

class QDaemonViewModel : public QAbstractListModel
{
public:
    QDaemonViewModel(QObject *parent=nullptr);
    int rowCount(const QModelIndex &) const;
    int columnCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    bool setHeaderData(int section, Qt::Orientation orientation, const QVariant &value, int role);
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    void populate(QList<SystemDaemon> *newValues);

private:
    QList<SystemDaemon>* values;
    QStringList headers;
};

#endif // QDAEMONVIEWMODEL_H
