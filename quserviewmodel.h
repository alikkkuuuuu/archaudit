#ifndef QUSERVIEWMODEL_H
#define QUSERVIEWMODEL_H

#include <QModelIndex>
#include <QStringList>
#include "lib/systemuser.h"

class QUserViewModel : public QAbstractListModel
{
public:
    QUserViewModel(QObject *parent=nullptr);
    int rowCount(const QModelIndex &) const;
    int columnCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    bool setHeaderData(int section, Qt::Orientation orientation, const QVariant &value, int role);
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    void populate(QList<SystemUser> *newValues);

private:
    QList<SystemUser>* values;
    QStringList headers;
};

#endif // QUSERVIEWMODEL_H
