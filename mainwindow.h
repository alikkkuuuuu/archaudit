#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QProcess>
#include <QMessageBox>

#include "lib/systeminformation.h"
#include "qjournalviewmodel.h"
#include <quserviewmodel.h>
#include <qdaemonviewmodel.h>
#include <qpackageviewmodel.h>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_updateListButton_clicked();

    void on_comboBox_currentTextChanged(const QString &arg1);

    void on_checkPwd_clicked();

    void on_encryptBtn_clicked();

    void on_portUpdBtn_clicked();

    void on_pushButton_clicked();

private:
    Ui::MainWindow *ui;
    SystemInformation* sysInfo;
    QUserViewModel* umodel;
    QDaemonViewModel* dmodel;
    QPackageViewModel* pmodel;
    qjournalviewmodel* jmodel;
};
#endif // MAINWINDOW_H
