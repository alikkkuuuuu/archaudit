#ifndef QJOURNALVIEWMODEL_H
#define QJOURNALVIEWMODEL_H


#include "qabstractitemmodel.h"
class qjournalviewmodel : public QAbstractListModel
{
public:
    qjournalviewmodel(QObject *parent=nullptr);
    int rowCount(const QModelIndex &) const;
    int columnCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    bool setHeaderData(int section, Qt::Orientation orientation, const QVariant &value, int role);
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    void populate(QString priority);

private:
    QList<QString>* values;
    QStringList headers;
};

#endif // QJOURNALVIEWMODEL_H
