#include "mainwindow.h"
#include "./ui_mainwindow.h"

QString formatUrl(QString name, QString url)
{
    return name + ": <a href=\"" + url + "\">" + url + "</a><br>";
}

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    sysInfo = new SystemInformation();

    ui->distribName->setText(sysInfo->DistribName);
    ui->host->setText((sysInfo->Host));
    ui->filesysTree->setText(sysInfo->FileTree);

    ui->auditStat->setText(sysInfo->Audit);

    ui->configFiles->setText(sysInfo->Configs);

    QString urlList;
    urlList.append(formatUrl("Home", sysInfo->DistribURL));
    urlList.append(formatUrl("Documentation", sysInfo->DocumentationURL));
    urlList.append(formatUrl("Support", sysInfo->SupportURL));
    urlList.append(formatUrl("Bug Report", sysInfo->BugReportURL));
    urlList.append(formatUrl("Privacy Policy", sysInfo->PrivacyPolicyURL));
    ui->urls->setText(urlList);

    ui->kernelVer->setText(sysInfo->KernelVer);

    umodel = new QUserViewModel();
    umodel->populate(sysInfo->getUsers());
    this->ui->userTable->setModel(umodel);
    this->ui->userTable->resizeColumnsToContents();

    dmodel = new QDaemonViewModel();
    dmodel->populate(sysInfo->getDaemons());
    this->ui->daemonsTable->setModel(dmodel);
    this->ui->daemonsTable->resizeColumnsToContents();

    pmodel = new QPackageViewModel();
    this->ui->packageTableView->setModel(pmodel);

    jmodel = new qjournalviewmodel();
    this->ui->journalView->setModel(jmodel);
    this->ui->comboBox->addItems(QStringList() << "kernel" << "emerg"
                                << "alert" << "crit" << "err" << "warning"
                                << "notice" << "info" << "debug");
}

MainWindow::~MainWindow()
{
    delete ui;
    delete sysInfo;
    delete umodel;
    delete dmodel;
    delete pmodel;
    delete jmodel;
}

void MainWindow::on_updateListButton_clicked()
{
    pmodel->populate();
    this->ui->packageTableView->resizeColumnsToContents();
}


void MainWindow::on_comboBox_currentTextChanged(const QString &arg1)
{
    jmodel->populate(this->ui->comboBox->currentText());
    this->ui->journalView->resizeColumnsToContents();
}


void MainWindow::on_checkPwd_clicked()
{
    QProcess proc;
    proc.start("sh", QStringList() << "-c" << "echo " + this->ui->pwdText->toPlainText() + " | cracklib-check");
    proc.waitForFinished();
    QString res = proc.readAllStandardOutput();
    this->ui->resultPwd->setText(res);
}


void MainWindow::on_encryptBtn_clicked()
{
    QProcess proc;
    proc.start("sh", QStringList() << "-c" << "echo " + this->ui->textEncrypt->toPlainText() +
                                          " | openssl aes-256-cbc -a -salt -pass pass:");
    proc.waitForFinished();
    this->ui->resEncrypt->setPlainText(proc.readAllStandardOutput());
}


void MainWindow::on_portUpdBtn_clicked()
{
    QProcess proc;
    proc.start("pkexec", QStringList() << "ss");
    proc.waitForFinished();
    this->ui->networkSet->setText(proc.readAllStandardOutput());
}


void MainWindow::on_pushButton_clicked()
{
    QProcess proc;
    proc.start("pkexec", QStringList() << "psad" << "--A");
    proc.waitForFinished();
    this->ui->diagRes->setText(proc.readAllStandardOutput());
}

