#include "qpackageviewmodel.h"
#include "qprocess.h"

QPackageViewModel::QPackageViewModel(QObject *parent)
    : QAbstractListModel(parent)
{
    values = new QList<Package>();
    headers << "Name";
}

int QPackageViewModel::rowCount(const QModelIndex &) const
{
    return values->count();
}

int QPackageViewModel::columnCount(const QModelIndex &parent) const
{
    return 1;
}

QVariant QPackageViewModel::data(const QModelIndex &index, int role) const
{
    QVariant value;
    switch(role)
    {
        case Qt::DisplayRole:
        {
            switch(index.column())
            {
                case 0:
                {
                    value = this->values->at(index.row()).Name;
                    break;
                }
            }
        }
        break;
        default:
        break;
    }

    return value;
}

bool QPackageViewModel::setHeaderData(int section, Qt::Orientation orientation, const QVariant &value, int role)
{
    return false;
}

QVariant QPackageViewModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::EditRole && role != Qt::DisplayRole)
        return QVariant();

    if (orientation == Qt::Horizontal)
    {
        if (section < headers.count())
            return QVariant(headers.at(section));
        else
            return QVariant(section + 1);
    }
    else
    {
        return QVariant(section + 1);
    }
}

void QPackageViewModel::populate()
{
    QProcess updProc;
    updProc.start("pkexec", QStringList() << "pacman" << "-Sy");
    updProc.waitForFinished();
    beginResetModel();
    QList<Package>* packages = new QList<Package>();
    QProcess proc;
    proc.start("pacman", QStringList() << "-Qu");
    proc.waitForFinished();
    QStringList packageList = QString(proc.readAllStandardOutput()).split('\n');
    packageList.removeAll({});
    for (auto &p : packageList)
    {
        Package np;
        np.Name = p;
        packages->append(np);
    }

    int idx = this->values->count();
    this->beginInsertRows(QModelIndex(), 1, idx);
    this->values = packages;
    endInsertRows();
    endResetModel();
}
