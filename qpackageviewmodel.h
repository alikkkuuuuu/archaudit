#ifndef QPACKAGEVIEWMODEL_H
#define QPACKAGEVIEWMODEL_H

#include <QModelIndex>
#include <QStringList>
#include "lib/package.h"

class QPackageViewModel : public QAbstractListModel
{
public:
    QPackageViewModel(QObject *parent=nullptr);
    int rowCount(const QModelIndex &) const;
    int columnCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    bool setHeaderData(int section, Qt::Orientation orientation, const QVariant &value, int role);
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    void populate();

private:
    QList<Package>* values;
    QStringList headers;
};

#endif // QPACKAGEVIEWMODEL_H
