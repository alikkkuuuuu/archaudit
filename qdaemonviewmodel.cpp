#include "qdaemonviewmodel.h"

QDaemonViewModel::QDaemonViewModel(QObject *parent)
    : QAbstractListModel(parent)
{
    values = new QList<SystemDaemon>();
    headers << "Name" << "Description" << "State" << "Preset";
}

int QDaemonViewModel::rowCount(const QModelIndex &) const
{
    return values->count();
}

int QDaemonViewModel::columnCount(const QModelIndex &parent) const
{
    return 4;
}

QVariant QDaemonViewModel::data(const QModelIndex &index, int role) const
{
    QVariant value;
    switch(role)
    {
        case Qt::DisplayRole:
        {
            switch(index.column())
            {
                case 0:
                {
                    value = this->values->at(index.row()).Name;
                    break;
                }
                case 1:
                {
                    value = this->values->at(index.row()).Description;
                    break;
                }
                case 2:
                {
                    value = this->values->at(index.row()).State;
                    break;
                }
                case 3:
                {
                    value = this->values->at(index.row()).Preset;
                    break;
                }
            }
        }
        break;
        default:
        break;
    }

    return value;
}

bool QDaemonViewModel::setHeaderData(int section, Qt::Orientation orientation, const QVariant &value, int role)
{
    return false;
}

QVariant QDaemonViewModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::EditRole && role != Qt::DisplayRole)
        return QVariant();

    if (orientation == Qt::Horizontal)
    {
        if (section < headers.count())
            return QVariant(headers.at(section));
        else
            return QVariant(section + 1);
    }
    else
    {
        return QVariant(section + 1);
    }
}

void QDaemonViewModel::populate(QList<SystemDaemon> *newValues)
{
    int idx = this->values->count();
    this->beginInsertRows(QModelIndex(), 1, idx);
    this->values = newValues;
    endInsertRows();
}
