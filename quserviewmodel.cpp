#include "quserviewmodel.h"
#include "qcolor.h"

QUserViewModel::QUserViewModel(QObject *parent)
    : QAbstractListModel(parent)
{
    values = new QList<SystemUser>();
    headers << "Login" << "UID" << "GID" << "Fullname" << "Home" << "Shell" << "Groups";
}

int QUserViewModel::rowCount(const QModelIndex &) const
{
    return values->count();
}

int QUserViewModel::columnCount(const QModelIndex &parent) const
{
    return 7;
}

QVariant QUserViewModel::data(const QModelIndex &index, int role) const
{
    QVariant value;
    switch(role)
    {
        case Qt::DisplayRole:
        {
            switch(index.column())
            {
                case 0:
                {
                    value = this->values->at(index.row()).Username;
                    break;
                }
                case 1:
                {
                    value = this->values->at(index.row()).UID;
                    break;
                }
                case 2:
                {
                    value = this->values->at(index.row()).GID;
                    break;
                }
                case 3:
                {
                    value = this->values->at(index.row()).Fullname;
                    break;
                }
                case 4:
                {
                    value = this->values->at(index.row()).HomeDirectory;
                    break;
                }
                case 5:
                {
                    value = this->values->at(index.row()).LoginShell;
                    break;
                }
                case 6:
                {
                    value = this->values->at(index.row()).Groups;
                    break;
                }
            }
        }
        break;

        case Qt::BackgroundRole:
        {
            if (this->values->at(index.row()).Groups.count(" ") > 2 ||
                this->values->at(index.row()).Username == "root")
            {
                value = QColor(127, 0, 0);
            }
        }
        default:
        break;
    }

    return value;
}

bool QUserViewModel::setHeaderData(int section, Qt::Orientation orientation, const QVariant &value, int role)
{
    return false;
}

QVariant QUserViewModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::EditRole && role != Qt::DisplayRole)
        return QVariant();

    if (orientation == Qt::Horizontal)
    {
        if (section < headers.count())
            return QVariant(headers.at(section));
        else
            return QVariant(section + 1);
    }
    else
    {
        return QVariant(section + 1);
    }
}

void QUserViewModel::populate(QList<SystemUser> *newValues)
{
    int idx = this->values->count();
    this->beginInsertRows(QModelIndex(), 1, idx);
    this->values = newValues;
    endInsertRows();
}
