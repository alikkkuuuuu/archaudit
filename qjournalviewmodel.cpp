#include "qjournalviewmodel.h"
#include "qprocess.h"

qjournalviewmodel::qjournalviewmodel(QObject *parent)
    : QAbstractListModel(parent)
{
    values = new QStringList();
    headers << "Name";
}

int qjournalviewmodel::rowCount(const QModelIndex &) const
{
    return values->count();
}

int qjournalviewmodel::columnCount(const QModelIndex &parent) const
{
    return 1;
}

QVariant qjournalviewmodel::data(const QModelIndex &index, int role) const
{
    QVariant value;
    switch(role)
    {
    case Qt::DisplayRole:
    {
        switch(index.column())
        {
        case 0:
        {
            value = this->values->at(index.row());
            break;
        }
        }
    }
    break;
    default:
        break;
    }

    return value;
}

bool qjournalviewmodel::setHeaderData(int section, Qt::Orientation orientation, const QVariant &value, int role)
{
    return false;
}

QVariant qjournalviewmodel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::EditRole && role != Qt::DisplayRole)
        return QVariant();

    if (orientation == Qt::Horizontal)
    {
        if (section < headers.count())
            return QVariant(headers.at(section));
        else
            return QVariant(section + 1);
    }
    else
    {
        return QVariant(section + 1);
    }
}

void qjournalviewmodel::populate(QString priority)
{
    QStringList command;
    beginResetModel();
    QProcess proc;
    if (priority == "kernel")
        command << "-k";
    else
        command << "-p" << priority << "-b";
    proc.start("journalctl", command);
    proc.waitForFinished();

    QStringList journal = QString(proc.readAllStandardOutput()).split('\n');
    int idx = this->values->count();
    this->beginInsertRows(QModelIndex(), 1, idx);
    this->values = new QStringList();
    this->values->append(journal);
    endInsertRows();
    endResetModel();
}
